#include "fatorial.h"

int fatorial(int x){
	/* Escreva seu código aqui */
	int fat, n;
	n = x;
	if (n<=0)
	{
		return -1;
	}	
	else
	{
		for (fat=1; n > 1; n--)
		{
			fat *= n;
		}
		return fat;
	}
}